# Proxmox Full-Clone
# ---
# Create a new VM from a clone

resource "proxmox_vm_qemu" "k3s-agent1" {
    
    # VM General Settings
    target_node = "pve"
    vmid = "8002"
    name = "k3s-agent1"
    desc = "k3s-agent1 node"

    # VM Advanced General Settings
    onboot = true 

    # VM OS Settings
    clone = "k3s-template"

    # VM System Settings
    agent = 1
    
    # VM CPU Settings
    cores = 4
    sockets = 1
    cpu = "host"    
    
    # VM Memory Settings
    memory = 8192

    # VM Network Settings
    network {
        bridge = "vmbr0"
        model  = "virtio"
    }

    # VM Cloud-Init Settings
    os_type = "cloud-init"

    # IP Address and Gateway
    ipconfig0 = "ip=192.168.50.221/24,gw=192.168.50.1"
    nameserver = "192.168.50.2"

    #  Default User
    ciuser = "pope"
    
    # Add your SSH KEY
    sshkeys = <<EOF
    ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIHtAw4kNtYZnNSFMYJ68Kj/xpquaLm6/XP82fuUWCChC
    EOF
}